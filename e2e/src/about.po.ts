import { browser, by, element } from 'protractor';

export class AboutPage {
    navigateTo() {
        return browser.get(browser.baseUrl) as Promise<any>;
    }
    browserDriverTime(time) {
        return browser.driver.sleep(time);
    }
    navigateToAbout() {
        return browser.get('/about');
    }
    getComments() {
        return element.all(by.css('.comments-li .comments-body'));
    }
    getLastElement() {
        return element.all(by.css('.comments-li')).last();
    }
    getInputText() {
        return element(by.css('.form-control'));
    }
    getAddComment() {
        return element(by.css('.add-comment'));
    }
    getDeleteComment() {
        this.browserDriverTime(4000);
        return element.all(by.css('.delete-comment')).last();
    }
}
