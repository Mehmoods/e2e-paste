import { browser, by, element, Key } from 'protractor';

export class PastePage {

    browserDriverTime(time) {
        return browser.driver.sleep(time);
    }
    navigateTo() {
        return browser.get(browser.baseUrl) as Promise<any>;
    }
    getLanguagesElements() {
        return element.all(by.css('.languages-tr'));
    }
    getOpenPasteModal() {
        return element(by.css('.languages-tr .view-paste'));
    }
    getClosePasteModal() {
        return element(by.css('.ant-modal-close'));
    }
    getOpenModalHeadingElement() {
        return element(by.css('.ant-modal-title div'));
    }
    getLanguageElements() {
        return element.all(by.css('.languages-tr .item'))
    }
    getOpenRandomModal() {
        return element(by.css('.view-paste-3'))
    }
    getRandomModalText() {
        return element.all(by.css('.item-3'))
    }
    getShowTextBtn() {
        return element(by.css('.show-btn'));
    }
    getShowText() {
        return element(by.css('.show-class'));
    }
    selectNextKey() {
        browser.actions().sendKeys(Key.ARROW_RIGHT).perform();
    }
    selectPrevKey() {
        browser.actions().sendKeys(Key.ARROW_LEFT).perform();
    }
}
