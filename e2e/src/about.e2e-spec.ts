import { AboutPage } from './about.po';

describe('workspace-project App', () => {
    let about: AboutPage;

    beforeEach(() => {
        about = new AboutPage();
    });

    it('should comments', () => {
        about.navigateTo();
        about.navigateToAbout();
        about.getComments().then((items) => {
            expect(about.getComments().count()).toBe(items.length);
        });
    });

    it('should add value', () => {
        const text = `random hush text`;
        about.getInputText().sendKeys(text);
        about.getAddComment().click();
        about.getComments().then((items) => {
            let item = items[items.length - 1];
            expect(item.getText()).toEqual(text);
        });
    });

    it('should delete value', () => {
        const text = `random hush text`;
        about.getDeleteComment().click();
        about.getComments().then((items) => {
            let item = items[items.length - 1];
            expect(item.getText()).not.toEqual(text);
        });
    });
});
