import { PastePage } from './paste.po';
import { browser, by, element, Key } from 'protractor';

describe('workspace-project App', () => {
    let page: PastePage;

    beforeEach(() => {
        page = new PastePage();
    });

    it('should display a list of paste', () => {
        page.navigateTo();
        expect(page.getLanguagesElements().count()).toBe(5);
    });

    it('should open modal and close', () => {
        page.navigateTo();
        page.getOpenPasteModal().click();
        page.getClosePasteModal().click();

        expect(page.getOpenPasteModal()).toBeTruthy();
        expect(page.getClosePasteModal()).toBeTruthy();
    });

    it('should get button element', () => {
        page.navigateTo();
        page.getOpenPasteModal().click();
        expect(page.getOpenModalHeadingElement().getText()).toBe(`Language C++`);
    });

    it('should get next heanding element', () => {
        page.navigateTo();
        page.getLanguageElements().then((items) => {
            expect(items[0].getText()).toBe('Hello World C++');
            expect(items[1].getText()).toBe('C++');
            expect(items[items.length - 1].getText()).toBe('Java');
            expect(items[items.length - 2].getText()).toBe('Hello World Java');
        });
    });

    it('should show button text is true', () => {
        page.getShowTextBtn().click();
        expect(page.getShowText().getText()).toEqual('Show Me!');
    });

    it('should open random modal', () => {
        page.getOpenRandomModal().click();
        page.getRandomModalText().then(items => {
            expect(items[0].getText()).toEqual('Hello World C');
            expect(items[1].getText()).toEqual('C');
        });
        page.getClosePasteModal().click();
    });
});
