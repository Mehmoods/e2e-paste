import { TestBed, getTestBed } from '@angular/core/testing';

import { ServerService } from './server.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('ServerService', () => {
    let service: ServerService;
    let httpMock: HttpTestingController;

    beforeEach(() => TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
    }));

    beforeEach(() => {
        service = TestBed.get(ServerService);
        httpMock = TestBed.get(HttpTestingController);
    });

    afterEach(() => {
        httpMock.verify();
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should http service observable', () => {
        service.getData().subscribe(data => {
            expect(data.length).toBe(5);
        });

        const request = httpMock.expectOne(`http://localhost:3000/posts`);
        expect(request.request.method).toBe('GET');
    });
});
