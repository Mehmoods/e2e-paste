import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Pastebin } from '../pastbeen/pastbin.model';
import { Comments } from '../comments.model';

@Injectable({
  providedIn: 'root'
})
export class ServerService {

  constructor(private http: HttpClient) { }

  getData(): Observable<Pastebin[]> {
    return this.http.get<Pastebin[]>('http://localhost:3000/posts');
  }

  getPaste(id) {
    return this.http.get(`http://localhost:3000/posts/${id}`);
  }

  getComments(): Observable<Comments[]> {
    return this.http.get<Comments[]>(`http://localhost:3000/comments`);
  }

  addComments(comment): Observable<Comments> {
    return this.http.post<Comments>(`http://localhost:3000/comments`, comment)
  }

  deleteComent(comment): Observable<Comments> {
    return this.http.delete<Comments>(`http://localhost:3000/comments/${comment.id}`);
  }
}
