import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { AddPaseComponent } from './add-pase/add-pase.component';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { PasteComponent } from './paste/paste.component';
import { AboutComponent } from './about/about.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    AddPaseComponent,
    PasteComponent,
    AboutComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgZorroAntdModule
  ],
  providers: [],
  entryComponents: [PasteComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
