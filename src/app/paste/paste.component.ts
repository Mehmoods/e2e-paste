import { Component, OnInit } from '@angular/core';
import { NzModalRef } from 'ng-zorro-antd';
import { ServerService } from '../services/server.service';

@Component({
  selector: 'app-paste',
  templateUrl: './paste.component.html',
  styleUrls: ['./paste.component.scss']
})
export class PasteComponent implements OnInit {
  paste = [];
  constructor(private modalRef: NzModalRef, private server: ServerService) { }

  ngOnInit() {
    const id = this.modalRef['nzComponentParams'];
    this.server.getPaste(id).subscribe(data => this.paste.push(data));
  }

}
