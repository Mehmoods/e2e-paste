import { Pastebin } from './pastbin.model';

describe('Pastebin', () => {
    it('should create an instance of Pastebin', () => {
        expect(new Pastebin()).toBeTruthy();
    });

    it('should accept values', () => {
        let pastebin = new Pastebin();
        pastebin = {
            id: 111,
            title: "Hello world",
            language: "Ruby"
        };
        expect(pastebin.id).toEqual(111);
        expect(pastebin.language).toEqual("Ruby");
    });
});
