import { Component, OnInit } from '@angular/core';
import { ServerService } from '../services/server.service';
import { Comments } from '../comments.model';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  comments: Comments[];
  comment: string;
  constructor(private service: ServerService) { }

  ngOnInit() {
    this.service.getComments()
      .subscribe(data => this.comments = data);
  }

  addComments() {
    this.service.addComments({ body: this.comment }).subscribe((data) => {
      this.comments.push(data);
    });
    this.comment = '';
  }

  deleteComment(comment) {
    this.service.deleteComent(comment).subscribe(() => {
      this.comments = this.comments.filter(item => item.id !== comment.id);
    });
  }
}
