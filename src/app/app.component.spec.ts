import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { By } from '@angular/platform-browser';

describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent,
        HeaderComponent
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
  });

  it('should create the app', () => {
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'e2e-test app is running!'`, () => {
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('e2e-test app is running!');
  });

  it(`should have main element in this elements have header and outlet`, () => {
    const de = fixture.debugElement.query(By.css('main'));
    console.log(fixture.debugElement.componentInstance === fixture.componentInstance);
    expect(de).toBeTruthy();
  });

});
