import { Component, OnInit } from '@angular/core';
import { ServerService } from '../services/server.service';
import { NzModalService } from 'ng-zorro-antd';
import { PasteComponent } from '../paste/paste.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  languages;
  showLanguages;

  constructor(private server: ServerService, private modalService: NzModalService) { }

  ngOnInit() {
    this.getData();
  }

  viewPasteModal(id, language) {
    this.modalService.create({
      nzTitle: `Language ${language}`,
      nzContent: PasteComponent,
      nzCancelText: 'Close',
      nzOkText: null,
      nzComponentParams: id
    });
  }

  getSow() {
    console.log(45)
  }

  getData() {
    this.server.getData()
      .subscribe(data => {
        this.showLanguages = true;
        this.languages = data;
      },
        (err) => {
          this.showLanguages = false;
          this.languages = 'Xuy Tebe!';
          console.log(err);
        });
  }

}
