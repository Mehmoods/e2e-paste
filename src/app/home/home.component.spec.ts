import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeComponent } from './home.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { NzModalService, NgZorroAntdModule } from 'ng-zorro-antd';
import { Pastebin } from '../pastbeen/pastbin.model';
import { ServerService } from '../services/server.service';

describe('HomeComponent', () => {
    let component: HomeComponent;
    let fixture: ComponentFixture<HomeComponent>;
    let de: DebugElement;
    let element: HTMLElement;
    let service: ServerService;
    let mockPaste: Pastebin[];
    let pastebinService: ServerService;
    let spy: jasmine.Spy;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule, NgZorroAntdModule],
            declarations: [HomeComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(HomeComponent);
        component = fixture.componentInstance;
        de = fixture.debugElement;
        service = TestBed.get(ServerService);
        element = de.nativeElement;
        fixture.detectChanges();
        mockPaste = [{
            id: 1,
            title: "Hello World C++",
            language: "C++"
        },
        {
            id: 2,
            title: "Hello World Ruby",
            language: "Ruby"
        },
        {
            id: 3,
            title: "Hello World C",
            language: "C"
        },
        {
            id: 4,
            title: "Hello World Javascript",
            language: "Javascript"
        },
        {
            id: 5,
            title: "Hello World Java",
            language: "Java"
        }];
    });

    it('should create', () => {
        const table = de.query(By.css('table'));
        expect(table).toBeTruthy();
    });

});
